#### Shower 
evgenConfig.description = 'Pythia_ttbar'
evgenConfig.keywords += ['ttbar','jets']
evgenConfig.contact  = [ "zach.marshall@cern.ch","giancarlo.panizzo@cern.ch" ]
evgenConfig.generators = ["Pythia8"]

evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1

#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version' : '3.0',
           'cut_decays'  : 'F',
           'drjj'        : 0.0,
           'mmll'        : 40,
           'ickkw'       : 0,
           'maxjetflavor': 5,
           'ktdurham'    : 30,
           'dparameter'  : 0.4
}

PYTHIA8_nJetMax=1
PYTHIA8_Process='pp>e+e-'
PYTHIA8_Dparameter=extras['dparameter']
PYTHIA8_TMS=extras['ktdurham']
PYTHIA8_nQuarksMerge=extras['maxjetflavor']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
