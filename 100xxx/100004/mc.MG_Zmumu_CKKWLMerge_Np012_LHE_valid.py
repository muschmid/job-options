import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

evgenConfig.nEventsPerJob = 10000

evgenConfig.description = 'MadGraph_ttbar'
evgenConfig.keywords += ['ttbar']
evgenConfig.contact  = [ "zach.marshall@cern.ch","giancarlo.panizzo@cern.ch" ]
evgenConfig.generators = ["MadGraph"]

nevents = runArgs.maxEvents*1.5 if runArgs.maxEvents>0 else 1.5*evgenConfig.nEventsPerJob

process_def = """
import model sm-no_b_mass
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
generate p p > mu+ mu- @0
add process p p > mu+ mu- j @1
add process p p > mu+ mu- j j @2
output -f"""

process_dir = new_process(process_def)
    
#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version' : '3.0', 
           'cut_decays'  : 'F', 
           'drjj'        : 0.0,
           'mmll'        : 40,
           'ickkw'       : 0,
           'maxjetflavor': 5,
           'ktdurham'    : 30,
           'dparameter'  : 0.4,
           'nevents'     : int(nevents)}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

generate(process_dir=process_dir,runArgs=runArgs)
# saveProcDir=True for testing only
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

# Reset to serial processing
check_reset_proc_number(opts)