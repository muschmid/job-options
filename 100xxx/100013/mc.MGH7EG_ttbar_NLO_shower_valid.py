#--------------------------------------------------------------
# Herwig7 (H72UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files

include("Herwig7_i/Herwig72_LHEF.py")

# initialize Herwig7 generator configuration for showering of LHE files

evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1

#lhe_filename = outputDS.split(".tar.gz")[0] + ".events"
lhe_filename = "LHE_events.events"

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=lhe_filename, me_pdf_order="NLO")

evgenConfig.description = 'Herwig7_ttbar'
evgenConfig.keywords += ['ttbar']
evgenConfig.contact  = [ "zach.marshall@cern.ch","giancarlo.panizzo@cern.ch" ]
evgenConfig.generators = ["MadGraph", "Herwig7", "EvtGen"]
evgenConfig.tune = "H7.2-Default"
# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")
# run Herwig7
Herwig7Config.run()