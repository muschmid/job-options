import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *

evgenConfig.description = 'MadGraph_ttbar'
evgenConfig.keywords += ['ttbar']
evgenConfig.contact  = [ "zach.marshall@cern.ch","giancarlo.panizzo@cern.ch" ]
evgenConfig.generators = ["MadGraph"]

evgenConfig.nEventsPerJob = 10000

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

### DSID lists (extensions can include systematics samples)
process_def = """
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > t t~
output -f"""

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version':'3.0',
             'cut_decays':'F',
             'nevents':int(nevents)}

process_dir = new_process(process_def)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)
# saveProcDir=True for testing only
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

# Reset to serial processing
check_reset_proc_number(opts)