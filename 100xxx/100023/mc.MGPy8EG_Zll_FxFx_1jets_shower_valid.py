# Shower
evgenConfig.description = 'Pythia8_ttbar'
evgenConfig.keywords+=['ttbar']
evgenConfig.contact  = [ "zach.marshall@cern.ch","giancarlo.panizzo@cern.ch" ]
evgenConfig.generators = ["Pythia8"]

#Madgraph run card and shower settings
# Shower/merging settings
maxjetflavor=5
parton_shower='PYTHIA8'
nJetMax=1
qCut=20.

# FxFx Matching settings, according to authors prescriptions (NB: it changes tune pars)
PYTHIA8_nJetMax=nJetMax
PYTHIA8_qCut=qCut

#### Shower: Py8 with A14 Tune, with FxFx prescription
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py") 
include("Pythia8_i/Pythia8_aMcAtNlo.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_FxFx_A14mod.py")

evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1