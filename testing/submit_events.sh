#!/bin/bash
#SBATCH --job-name=testing             # Job name
#SBATCH --nodes=1                      # Run on 1 node
#SBATCH --ntasks=1                     # Run on 32 CPUs
#SBATCH --mem=2gb                      # Job memory request
#SBATCH --time=48:00:00                # Time limit hrs:min:sec
#SBATCH --output=logs/testing_%j.log   # Standard output and error log

#SBATCH --mail-user=muschmidt@uni-wuppertal.de # Send E-Mails
#SBATCH --mail-type=BEGIN,END,FAIL,REQUEUE,ARRAY_TASKS

nevents=10000

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup AthGeneration,23.6.23

cd ${TESTFOLDER}

num1=$1 # Even numbers
# Running MG
rm -r ${MCFOLDER}/$num1
rm -r ${TESTFOLDER}/$num1
mkdir ${num1}
cd ${num1}
echo "Starting run ${num1}"
Gen_tf.py  --ecmEnergy=13000 --firstEvent=1 --maxEvents=$nevents --randomSeed=1234 --jobConfig=${CONFIGFOLDER}/${num1} --outputTXTFile=LHE_events
wait
cp log.generate ${CONFIGFOLDER}/${num1}/
cp -r ${CONFIGFOLDER}/${num1} ${MCFOLDER}/
