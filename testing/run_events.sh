# Set up folders
export BASEFOLDER=/beegfs/schmidt

folder=$PWD

export CONFIGFOLDER=${BASEFOLDER}/job-options/100xxx
export TESTFOLDER=${BASEFOLDER}/job-testing/100xxx
export MCFOLDER=${BASEFOLDER}/mcjoboptions/100xxx

mkdir -p ${TESTFOLDER}

#rm -r ${TESTFOLDER}/*
#rm -r ${MCFOLDER}/*

mkdir -p ${MCFOLDER}

# Run over all job options
for i in $(seq 100000 2 100023)
do
    sbatch submit_events.sh $i
done
