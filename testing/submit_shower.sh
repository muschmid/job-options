#!/bin/bash
#SBATCH --job-name=testing             # Job name
#SBATCH --nodes=1                      # Run on 1 node
#SBATCH --ntasks=1                     # Run on 32 CPUs
#SBATCH --mem=2gb                      # Job memory request
#SBATCH --time=48:00:00                # Time limit hrs:min:sec
#SBATCH --output=logs/testing_%j.log   # Standard output and error log

#SBATCH --mail-user=muschmidt@uni-wuppertal.de # Send E-Mails
#SBATCH --mail-type=BEGIN,END,FAIL,REQUEUE,ARRAY_TASKS

nevents=10000

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup AthGeneration,23.6.23

cd ${TESTFOLDER}

num1=$1 # Even numbers
num2=$((num1 + 1)) # Odd numbers
# LHE Showering
rm -r ${MCFOLDER}/$num2
rm -r ${TESTFOLDER}/$num2
mkdir ${num2}
cd ${num2}
cp ../${num1}/*LHE_events* .
echo "Starting run ${num2}"
Gen_tf.py  --ecmEnergy=13000 --firstEvent=1 --maxEvents=${nevents} --randomSeed=1234 --jobConfig=${CONFIGFOLDER}/${num2} --outputEVNTFile=output.root --inputGeneratorFile=LHE_events
cp log.generate ${CONFIGFOLDER}/${num2}/
cp -r ${CONFIGFOLDER}/${num2} ${MCFOLDER}/
